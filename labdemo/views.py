from rest_framework.views import APIView
from labdemo.models import Venue, VenueTips
from labdemo.serializers import venueSerializer
from rest_framework.response import Response
from django.views.generic import TemplateView
from django.views.decorators.clickjacking import xframe_options_exempt
from django.utils.decorators import method_decorator
from itertools import chain
from operator import attrgetter

class SearchView(APIView):
    @method_decorator(xframe_options_exempt)
    def get(self, request):
        term = request.GET.get('text')
        addrs = Venue.es_search(term)
        print '================================ Venues ================================'
        print addrs


        addrs2 = VenueTips.es_search(term)
    	venueTips = Venue.objects.filter(id__in=addrs2)
        print '================================ Venue Tips ================================'
        print venueTips

        allVenues = chain(addrs, venueTips)
        venue_serializer = venueSerializer(allVenues, many=True)
        response = {}
        response['addrs'] = venue_serializer.data
        return Response(response)


class IndexView(TemplateView):
    template_name = 'index.html'

class LeafletView(TemplateView):
    template_name = 'leaflet.html'