# -*- coding: utf-8 -*-
import requests
import json
import traceback
import time
import sys
import psycopg2
import threading
import urllib
import multiprocessing



## ------------------------>>>>> Parser WITHOUT CATEGORIES <<<<<-------------------------------------------


clientID = 'ZGNTTU25T2VSIDTZP34LQWHYSWKWJIHUSPBS3NP0LWSK22EO' # Вставить сюда свой client_id
clientSecret = 'UO54ZTTSM2FTVINGJ34VPLQMHC0SYRZNZKCQV0M1K2AJLRZA' # Вставить сюда свой client_secret
limit = 20 # Можно с помошью этой переменной управлять количеством записей на одной странице вывода данных
v = '20160901' # Версия json
near = 'Алматы,Казахстан' # Где искать
query = 'restaurant' # Запрос
venues = []
reviews = []

def getVenuesCount():
	global clientID, clientSecret, v, near, query
	totalResults = 0
	url = 'https://api.foursquare.com/v2/venues/explore'
	params = urllib.urlencode({'client_id': clientID,
							  'client_secret': clientSecret,
							  'v': v,
							  'near': near,
							  'query': query,
							  'limit': 1})

	result = requests.get(url + '?' + params)
	if result:
		data = json.loads(result.text)
		if 'response' in data:
			if 'totalResults' in data['response']:
				totalResults = data['response']['totalResults']
	return totalResults

# Вытаскиваем количество необходимых страниц
venuesCount = getVenuesCount()
if int(venuesCount) % limit == 0:
	processNumber = (int(venuesCount)/limit)
else:
	processNumber = (int(venuesCount)/limit) + 1

def getVenues(offset):
	global clientID, clientSecret, limit, v, near, query, limit, venues
	url = 'https://api.foursquare.com/v2/venues/explore'
	params = urllib.urlencode({'client_id': clientID,
							  'client_secret': clientSecret,
							  'v': v,
							  'near': near,
							  'query': query,
							  'limit': limit,
							  'offset': offset})

	result = requests.get(url + '?' + params)
	if result:
		data = json.loads(result.text)
		if 'response' in data:
			if 'groups' in data['response']:
				groups = data['response']['groups']
				for group in groups:
					if 'items' in group:
						items = group['items']
						for item in items:
							if 'venue' in item:
								venueID = item['venue']['id']
								name = item['venue']['name']
								lon = item['venue']['location']['lng']
								lat = item['venue']['location']['lat']
								venues.append({'id': venueID,
											   'name': name,
											   'lon': lon,
											   'lat': lat})
					break
			else:
				print(result.text)
				sys.exit()
		else:
			print(result.text)
			sys.exit()
	else:
		print(result)
		sys.exit()
	print '============================ Process %s ===============================' % (offset/20)


def getReviews(venues):
	global clientID, clientSecret, v, reviews
	for venue in venues:
		print '============================ Venue %s ===============================' % (venue['id'])
		url = 'https://api.foursquare.com/v2/venues/%s/tips' % venue['id']
		params = {'client_id': clientID,
				  'client_secret': clientSecret,
				  'v': v,
				  'limit': 500}
		r = requests.get(url, params=params)
		if r:
			data = r.json()
			if 'response' in data:
				if 'tips' in data['response']:
					if 'items' in data['response']['tips']:
						tips = data['response']['tips']['items']
						for tip in tips:
							tipID = tip['id']
							text = tip['text']
							reviews.append({'id': tipID,
											'text': text,
											'venue_id':venue['id']})

def writeVenuesToDB(venues):
	connection = psycopg2.connect(host = 'localhost', database = 'labdemo_test', user = 'viccio', password = 'Fnnhb,en56', port = '5432')
	try:
		cursor = connection.cursor()
		for venue in venues:
			cursor.execute('SELECT count(*) FROM labdemo_venue WHERE id like \'' + venue['id'] + '\'' )
			existanceCheck = cursor.fetchone()
			if existanceCheck[0] == 0:
				cursor.execute('INSERT INTO labdemo_venue (id, name, latitude, longitude) ' + \
						 	'VALUES (%s, %s, %s, %s) ',
						 	(venue['id'], venue['name'], venue['lon'], venue['lat']))
				print venue['name'] + ' successfully inserted to database'
			else:
				print 'ERROR: ' + venue['name'] + '-------> VenueTip is already in database'



		cursor.execute('UPDATE labdemo_venue SET point = ST_SetSRID(ST_MakePoint(longitude, latitude), 4326);')
		connection.commit()

	except:
		print(traceback.format_exc())
		connection.rollback()
	finally:
		connection.close()




def writeReviewsToDB(reviews):
	connection = psycopg2.connect(host = 'localhost', database = 'labdemo_test', user = 'viccio', password = 'Fnnhb,en56', port = '5432')
	try:
		cursor = connection.cursor()
		for review in reviews:
			cursor.execute('SELECT count(*) FROM labdemo_venuetips WHERE tip_id like \'' + review['id'] + '\'' )
			existanceCheck = cursor.fetchone()
			if existanceCheck[0] == 0:
				cursor.execute('INSERT INTO labdemo_venuetips (tip_id, tip_text, venue_id) ' + \
						 	'VALUES (%s, %s, %s) ',
						 	(review['id'], review['text'], review['venue_id']))
				print 'Successfully inserted VenueTip with ID: ---->' + review['id'] +  ' to database'
			else:
				print 'ERROR: ' + review['id'] + ' -------> VenueTip with this ID already exist in database'


		connection.commit()

	except:
		print(traceback.format_exc())
		connection.rollback()
	finally:
		connection.close()



def main():
	timeStart = time.time()
	global venues, reviews
	threadVenues = []
	for i in range(processNumber):
		t = threading.Thread(target=getVenues, args=(i*20,))
		threadVenues.append(t)
		t.start()
	for t in threadVenues:
		t.join()

	threadReviews = []
	for i in range(processNumber):
		if i != processNumber-1:
			t = threading.Thread(target=getReviews, args=(venues[i*20:(i+1)*20],))
		else:
			t = threading.Thread(target=getReviews, args=(venues[i*20:],))
		threadReviews.append(t)
		t.start()
	for t in threadReviews:
		t.join()

	writeVenuesToDB(venues)
	writeReviewsToDB(reviews)

	print len(venues)
	print len(reviews)
	print("TIME:   ", time.time() - timeStart)
if __name__ == '__main__':
	main()
